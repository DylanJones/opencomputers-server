from flask import Flask

app = Flask(__name__)


@app.route('/drone/test')
def test():
    return "Hello there drone"

@app.route('/')
def readme():
    return "This is not a server for humans.  Go away, human."

if __name__ == '__main__':
    app.run()
